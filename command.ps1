#minikube start --memory=4096 --cpus=4

#create namespace for kafka cluster
kubectl create ns kafka
kubectl create ns my-kafka-project

#setup SQL server
Set-Location D:\Projects\YearPlan\pof-kafka-sqlserver
.\sqlserver\setup.ps1

#setup Kafka cluster
Set-Location D:\Projects\YearPlan\pof-kafka-sqlserver
.\strimzi-kafka\setup.ps1

kubectl rollout status deployment strimzi-cluster-operator --timeout=600s 

#setup KSQL Server
Set-Location D:\Projects\YearPlan\pof-kafka-sqlserver
.\cp-helm-charts\charts\cp-schema-registry\setup.ps1

Set-Location D:\Projects\YearPlan\pof-kafka-sqlserver
.\cp-helm-charts\charts\cp-ksql-server\setup.ps1