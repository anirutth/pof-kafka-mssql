# set current dir
Set-Location strimzi-kafka
#replace "myproject" in 
#./strimzi-0.22.1/install/cluster-operator/*RoleBinding*.yaml
#Edit the install/cluster-operator/060-Deployment-strimzi-cluster-operator.yaml file and set the STRIMZI_NAMESPACE environment variable to the namespace my-kafka-project
#   env:
#   - name: STRIMZI_NAMESPACE
#     value: my-kafka-project


kubectl create -f .\install\cluster-operator\ -n kafka
kubectl create -f .\install\cluster-operator\020-RoleBinding-strimzi-cluster-operator.yaml -n my-kafka-project
kubectl create -f .\install\cluster-operator\032-RoleBinding-strimzi-cluster-operator-topic-operator-delegation.yaml -n my-kafka-project
kubectl create -f .\install\cluster-operator\031-RoleBinding-strimzi-cluster-operator-entity-operator-delegation.yaml -n my-kafka-project
kubectl rollout -n kafka status deployment strimzi-cluster-operator --timeout=600s 

#create cluster
kubectl create -n my-kafka-project -f my-cluster.yaml
kubectl wait kafka/my-cluster --for=condition=Ready --timeout=300s -n my-kafka-project

#create topic
kubectl create -n my-kafka-project -f my-topic.yaml

#run producer
#kubectl -n my-kafka-project run kafka-producer -ti --image=strimzi/kafka:0.20.1-kafka-2.5.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --broker-list my-cluster-kafka-bootstrap:9092 --topic my-topic

#run consumer
#kubectl -n my-kafka-project run kafka-consumer -ti --image=strimzi/kafka:0.20.1-kafka-2.5.0 --rm=true --restart=Never -- bin/kafka-console-consumer.sh --bootstrap-server my-cluster-kafka-bootstrap:9092 --topic my-topic --from-beginning

#install schema registry
#helm install kafka-schema-registry --set kafka.bootstrapServers="PLAINTEXT://my-cluster-kafka-bootstrap:9092" . -n my-kafka-project


#install KSQL Server
#helm install ksql-server --set cp-schema-registry.url="http://kafka-schema-registry-cp-schema-registry:8081",kafka.bootstrapServers="PLAINTEXT://my-cluster-kafka-bootstrap:9092",ksql.headless=false . -n my-kafka-project


#kubectl -n my-kafka-project run tmp-ksql-cli --rm -i --tty --image confluentinc/cp-ksql-cli:5.2.1 http://ksql-server-cp-ksql-server:8088