use master
GO

CREATE DATABASE [bnd_test]
GO

USE [bnd_test]
GO

EXEC sys.sp_cdc_enable_db  
GO  

/****** Object:  Table [dbo].[M_ApplicationType]    Script Date: 3/30/2021 10:19:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_ApplicationType](
	[AppTypeID] [uniqueidentifier] NOT NULL,
	[AppTypeName] [nvarchar](100) NULL,
	[AppTypeDesc] [nvarchar](255) NULL,
	[AppTypeOrderItem] [int] NULL,
	[Status] [bit] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [smalldatetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[AppType_PathImg] [nvarchar](255) NULL,
	[AppType_Color] [nvarchar](100) NULL,
 CONSTRAINT [PK_M_ApplicationType] PRIMARY KEY CLUSTERED 
(
	[AppTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_cdc_enable_table  
    @source_schema = N'dbo',  
    @source_name   = N'M_ApplicationType',  
    @role_name     = NULL,  
    @filegroup_name = NULL,  
    @supports_net_changes = 1 
GO

EXEC sys.sp_cdc_help_change_data_capture 
	@source_schema = 'dbo', 
	@source_name = 'M_ApplicationType'
GO 