#Set-Location sqlserver
kubectl -n my-kafka-project create secret generic mssql --from-literal=SA_PASSWORD="MyC0m9l&xP@ssw0rd"
kubectl -n my-kafka-project apply -f sqlserver-deployment.yaml

kubectl -n my-kafka-project rollout status deployment mssql-deployment --timeout=90s

minikube docker-env | Invoke-Expression
docker build -t sqlserver-image:1.0 .

kubectl -n my-kafka-project run sqlserver-image -ti --image=sqlserver-image:1.0 --rm=true --restart=Never -- /opt/mssql-tools/bin/sqlcmd -S mssql-deployment -U SA -P "MyC0m9l&xP@ssw0rd" -i ./sql/CreateDatabase.sql

kubectl -n my-kafka-project run sqlserver-image -ti --image=sqlserver-image:1.0 --rm=true --restart=Never -- /opt/mssql-tools/bin/sqlcmd -S mssql-deployment -U SA -P "MyC0m9l&xP@ssw0rd" -i ./sql/InsertData.sql

#kubectl -n my-kafka-project run sqlserver-image -ti --image=sqlserver-image:1.0 --rm=true --restart=Never -- /opt/mssql-tools/bin/sqlcmd -S 10.111.230.60 -U SA -P "MyC0m9l&xP@ssw0rd" -q "SELECT * FROM [dbo].[M_ApplicationType]"